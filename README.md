## Muffin Docker Compose App 

This will be an app to showcase docker compose. We'll build it with a php html front page and another container with a python API simulating a DB.

We'll use this class to cover:
- Dev environments
- Testing and CD enviroments
- Git and github branching
- Git branching practices and etiquette
- Docker and docker compose


Will also touch on:

- Python
- APIs
- Webapps
- Seperation of concerns

Extra objectives: 

- add CI 
- add CD


### Plan to complete:

1. Create simple API with python. Then put it in a container. 
2. Create simple PHP app to parse/consume JSON sent from API.
3. Build our docker compose

### API

testing CI- Nicole
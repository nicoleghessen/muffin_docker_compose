from flask import Flask
from flask_restful import Resource, Api


app = Flask(__name__)
api = Api(app)
# print(app)
# print(api)

# code our Flask API
# we need muffins to be supplied as JSON
class Product(Resource):
    def get(self):
        return {
            "products": ["beer induced muffin", "double chocolate", "Duffin!", "Raisin Muffin"]
        }

class Product_price(Resource):
    def get(self):
        return {
            "price": [12, 10, 17, 10]
        }




# Expose this in the API
api.add_resource(Product, '/')
api.add_resource(Product_price, '/price')

if __name__ == '__main__':
    print("THIS IS THE MAIN!")
    print("use this block to make an app have different nehaviour when called directly (clicked on or by python)")
    app.run("0.0.0.0", port=80, debug=True)